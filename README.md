### Build & Run ###
##### Using java without jar #####
```
mkdir ./target
javac -sourcepath src/main/java/ -d ./target -Xlint src/main/java/com/nab/harry/Application.java
java -classpath ./target com.nab.harry.Application
```

##### Or using Maven #####
```
mvn install
java -jar target/tradereport-0.1.0.jar
```

### Tech Stack ###
- JDK 8.0
- Unit test (JUnit and mockito)
