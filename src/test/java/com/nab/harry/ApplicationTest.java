package com.nab.harry;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ApplicationTest {

    @Test
    public void testMain() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        try {
            Application.main(new String[]{});
        } catch (Throwable t) {
            assertNull(t);
        }
        System.setOut(System.out);
        String result = outContent.toString();
        assertEquals(result, TestData.EXPECTED_RESULT);
    }
}
