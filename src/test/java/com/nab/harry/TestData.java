package com.nab.harry;

public class TestData {
    public final static String EXPECTED_RESULT = "buyer_party,seller_party,premium_amount,premium_currency\n"
            + "LEFT_BANK,EMU_BANK,      100.00,AUD\n"
            + "LEFT_BANK,EMU_BANK,      200.00,AUD\n"
            + "EMU_BANK,BISON_BANK,      500.00,USD\n"
            + "EMU_BANK,BISON_BANK,      600.00,USD\n";
}
