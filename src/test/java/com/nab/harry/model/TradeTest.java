package com.nab.harry.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TradeTest {

    @Test
    public void testSetGets() {
        Trade t = new Trade();
        t.setBuyerPartyReference("LEFT_BANK");
        t.setSellerPartyReference("EMU_BANK");
        t.setPaymentCurrency("AUD");
        t.setPaymentAmount(100.0);
        assertEquals(t.getBuyerPartyReference(), "LEFT_BANK");
        assertEquals(t.getSellerPartyReference(), "EMU_BANK");
        assertEquals(t.getPaymentCurrency(), "AUD");
        assertEquals(t.getPaymentAmount(), 100.0, 0.01);
    }

    @Test
    public void testHashCodeAndEquals() {
        Trade t1 = new Trade("LEFT_BANK", "EMU_BANK", "AUD", 100.0);
        Trade t2 = new Trade("LEFT_BANK", "EMU_BANK", "AUD", 100.1);
        Trade t3 = new Trade("LEFT_BANK", "EMU_BANK", "AUD", 100.0);
        Trade t4 = t2;
        assertNotEquals(t1.hashCode(), t2.hashCode());
        assertNotEquals(t1, t2);
        assertNotEquals(t1, null);
        assertNotEquals(t1, new Object());
        assertEquals(t1, t3);
        assertEquals(t2, t4);
    }
}
