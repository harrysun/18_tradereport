package com.nab.harry;

import com.nab.harry.model.Trade;
import org.junit.Test;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TradeReporterTest {

    @Test
    public void testReport() {
        StringWriter w = new StringWriter();
        TradeReporter reporter = new TradeReporter(w);
        List<Trade> trades = new ArrayList<>();
        trades.add(new Trade("LEFT_BANK", "EMU_BANK", "AUD", 100.00));
        trades.add(new Trade("LEFT_BANK", "EMU_BANK", "AUD", 200.00));
        trades.add(new Trade("EMU_BANK", "BISON_BANK", "USD", 500.00));
        trades.add(new Trade("EMU_BANK", "BISON_BANK", "USD", 600.00));
        reporter.report(trades);
        assertEquals(w.getBuffer().toString(), TestData.EXPECTED_RESULT);
    }
}
