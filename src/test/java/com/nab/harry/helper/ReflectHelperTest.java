package com.nab.harry.helper;

import com.nab.harry.model.Trade;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Date;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class ReflectHelperTest {

    @Test
    public void testNewInstance() {
        Object obj = ReflectHelper.newInstance(Trade.class);
        assertNotNull(obj);
        assertTrue(obj instanceof Trade);
    }

    @Test
    public void testFindFields() {
        Field[] fields = ReflectHelper.findFields(Trade.class, XmlPath.class);
        Field[] fields2 = null;
        try {
            fields2 = new Field[]{
                    Trade.class.getDeclaredField("buyerPartyReference"),
                    Trade.class.getDeclaredField("sellerPartyReference"),
                    Trade.class.getDeclaredField("paymentAmount"),
                    Trade.class.getDeclaredField("paymentCurrency"),
            };
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
        assertArrayEquals(fields, fields2);
    }

    @Test
    public void testConvert() {
        assertEquals(ReflectHelper.convert("Hello", String.class), "Hello");
        assertEquals(ReflectHelper.convert("1.23", Double.class), 1.23, 0.01);
        assertEquals(ReflectHelper.convert("16/03/2018", Date.class), "16/03/2018");
    }
}
