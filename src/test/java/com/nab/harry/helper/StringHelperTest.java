package com.nab.harry.helper;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class StringHelperTest {

    @Test
    public void testAnagram() {
        assertTrue(StringHelper.anagram(null, null));
        assertTrue(StringHelper.anagram("", ""));
        assertTrue(StringHelper.anagram("act", "cat"));
        assertTrue(StringHelper.anagram("Elbow", "Below"));
        assertFalse(StringHelper.anagram("abc", "abc "));
        assertFalse(StringHelper.anagram("abc", null));
    }
}
