package com.nab.harry.helper;

import com.nab.harry.model.Trade;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class XmlParserTest {

    @Test
    public void testParseXml() {
        Trade t = XmlParser.parseXml(new File("data/event0.xml"), Trade.class);
        Trade t2 = new Trade("LEFT_BANK", "EMU_BANK", "AUD", 100.0);
        assertEquals(t, t2);
    }
}
