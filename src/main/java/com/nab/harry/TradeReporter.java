package com.nab.harry;

import com.nab.harry.model.Trade;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

public class TradeReporter {

    private final static String[] HEADERS = {
            "buyer_party",
            "seller_party",
            "premium_amount",
            "premium_currency"
    };

    private final static String HEADER_FORMAT = "%s,%s,%s,%s\n";
    private final static String ROW_FORMAT = "%s,%s,%12.2f,%s\n";

    private Writer writer;

    public TradeReporter() {
        this(new PrintWriter(System.out));
    }

    public TradeReporter(Writer writer) {
        this.writer = writer;
    }

    public void report(List<Trade> trades) {
        try {
            writer.write(String.format(HEADER_FORMAT, HEADERS));
            for (Trade trade : trades) {
                writer.write(String.format(ROW_FORMAT, trade.getBuyerPartyReference(), trade.getSellerPartyReference(),
                        trade.getPaymentAmount(), trade.getPaymentCurrency()));
            }
            writer.flush();
            writer.close();
        } catch (Exception ex) {
            throw new ApplicationException(ex);
        }
    }
}
