package com.nab.harry.helper;

import com.nab.harry.ApplicationException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ReflectHelper {

    private ReflectHelper() {
    }

    public static <T> T newInstance(Class<T> targetClass) {
        try {
            return targetClass.getDeclaredConstructor().newInstance();
        } catch (Exception ex) {
            throw new ApplicationException(ex);
        }
    }

    public static Field[] findFields(Class<?> targetClass, Class<? extends Annotation> annotationCls) {
        Field[] allFields = targetClass.getDeclaredFields();
        List<Field> annotatedFields = new ArrayList();
        Field[] arr = allFields;
        int len = allFields.length;

        for (int i = 0; i < len; ++i) {
            Field field = arr[i];
            if (field.getAnnotation(annotationCls) != null) {
                field.setAccessible(true);
                annotatedFields.add(field);
            }
        }
        return annotatedFields.toArray(new Field[annotatedFields.size()]);
    }

    // only provided few converters, which is good enough for the exercise.
    // in real report engine, we can leverage third party libraries!
    public static <T> T convert(String value, Class<T> targetClass) {
        if (String.class.equals(targetClass)) {
            return (T) value;
        } else if (Double.class.equals(targetClass)) {
            return (T) new Double(Double.parseDouble(value));
        } else {
            return (T) value;
        }
    }

}
