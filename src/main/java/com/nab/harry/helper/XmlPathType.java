package com.nab.harry.helper;

import javax.xml.namespace.QName;

import static javax.xml.xpath.XPathConstants.BOOLEAN;
import static javax.xml.xpath.XPathConstants.NODE;
import static javax.xml.xpath.XPathConstants.NODESET;
import static javax.xml.xpath.XPathConstants.NUMBER;
import static javax.xml.xpath.XPathConstants.STRING;

public enum XmlPathType {

    X_NUMBER(NUMBER),
    X_STRING(STRING),
    X_BOOLEAN(BOOLEAN),
    X_NODESET(NODESET),
    X_NODE(NODE);

    private QName type;

    XmlPathType(QName type) {
        this.type = type;
    }

    public QName getType() {
        return type;
    }
}
