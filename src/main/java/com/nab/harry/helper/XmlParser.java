package com.nab.harry.helper;

import com.nab.harry.ApplicationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.lang.reflect.Field;

public class XmlParser {

    private XmlParser() {
    }

    public static <T> T parseXml(final File file, final Class<T> targetClass) {
        assert file != null;
        assert targetClass != null;
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document document = builder.parse(file);
            T t = ReflectHelper.newInstance(targetClass);
            Field[] fs = ReflectHelper.findFields(targetClass, XmlPath.class);
            XPath xPath = XPathFactory.newInstance().newXPath();
            for (Field f : fs) {
                XmlPath xp = f.getAnnotation(XmlPath.class);
                NodeList nodeList = (NodeList) xPath.compile(xp.path()).evaluate(document, xp.type().getType());
                assert nodeList.getLength() == 1;
                Object value = ReflectHelper.convert(nodeList.item(0).getNodeValue(), f.getType());
                f.setAccessible(true);
                f.set(t, value);
            }
            return t;
        } catch (Exception ex) {
            throw new ApplicationException(ex);
        }
    }

}
