package com.nab.harry.helper;

import java.util.HashMap;
import java.util.Map;

public class StringHelper {

    private StringHelper() {
    }

    public static boolean anagram(String s, String t) {
        if (s == null && t == null) {
            return true;
        }
        // Strings of unequal lengths can't be anagrams
        if (s == null || t == null || s.length() != t.length()) {
            return false;
        }

        // They're anagrams if both produce the same 'frequency map'
        return frequencyMap(s).equals(frequencyMap(t));
    }

    /**
     * For example, returns `{b=3, c=1, a=2}` for the string "aabcbb"
     * @param str
     * @return
     */
    private static Map<Character, Integer> frequencyMap(String str) {
        Map<Character, Integer> map = new HashMap<>();
        for (char c : str.toLowerCase().toCharArray()) {
            Integer frequency = map.get(c);
            map.put(c, frequency == null ? 1 : frequency + 1);
        }
        return map;
    }
}
