package com.nab.harry.helper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.nab.harry.helper.XmlPathType.X_NODE;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface XmlPath {

    String path();

    XmlPathType type() default X_NODE;
}
