package com.nab.harry;

import com.nab.harry.helper.StringHelper;
import com.nab.harry.model.Trade;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.nab.harry.helper.XmlParser.parseXml;

public class Application {

    private final static String DEFAULT_DATA_DIR = "./data/";

    // Harry Sun
    public static void main(String[] args) {
        Predicate<Trade> sellerIsEmuBank = t -> "EMU_BANK".equals(t.getSellerPartyReference());
        Predicate<Trade> sellerIsBisonBank = t -> "BISON_BANK".equals(t.getSellerPartyReference());
        Predicate<Trade> currencyIsAUD = t -> "AUD".equals(t.getPaymentCurrency());
        Predicate<Trade> currencyIsUSD = t -> "USD".equals(t.getPaymentCurrency());
        Predicate<Trade> sellerAndBuyerAreNotAnagrams = t -> !StringHelper.anagram(t.getSellerPartyReference(), t.getBuyerPartyReference());
        Predicate<Trade> reportCriteria = ((sellerIsEmuBank.and(currencyIsAUD))
                .or(sellerIsBisonBank.and(currencyIsUSD))).and(sellerAndBuyerAreNotAnagrams);
        String dir = args.length > 0 ? args[0] : DEFAULT_DATA_DIR;
        File dd = new File(dir);
        File[] xmls = dd.listFiles(fnf -> fnf.getName().endsWith(".xml"));
        try {
            List<Trade> trades = new ArrayList<>();
            for (File f: xmls) {
                try { // if the xml is not valid, then ignore and skip to next
                    trades.add(parseXml(f, Trade.class));
                } catch (Exception ex) {
                    ex.printStackTrace(System.err);
                }
            }
            List<Trade> filteredTrades = trades.stream().filter(reportCriteria).collect(Collectors.toList());
            TradeReporter reporter = new TradeReporter();
            reporter.report(filteredTrades);
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }
}
