package com.nab.harry.model;

import com.nab.harry.helper.XmlPath;

import static com.nab.harry.helper.XmlPathType.X_NODESET;

public class Trade {

    @XmlPath(path = "//buyerPartyReference/@href", type = X_NODESET)
    private String buyerPartyReference;

    @XmlPath(path = "//sellerPartyReference/@href", type = X_NODESET)
    private String sellerPartyReference;

    @XmlPath(path = "//paymentAmount/amount")
    private Double paymentAmount;

    @XmlPath(path = "//paymentAmount/currency")
    private String paymentCurrency;

    public Trade() {
    }

    public Trade(String buyerPartyReference, String sellerPartyReference, String paymentCurrency, Double paymentAmount) {
        this.buyerPartyReference = buyerPartyReference;
        this.sellerPartyReference = sellerPartyReference;
        this.paymentCurrency = paymentCurrency;
        this.paymentAmount = paymentAmount;
    }

    public String getBuyerPartyReference() {
        return buyerPartyReference;
    }

    public void setBuyerPartyReference(String buyerPartyReference) {
        this.buyerPartyReference = buyerPartyReference;
    }

    public String getSellerPartyReference() {
        return sellerPartyReference;
    }

    public void setSellerPartyReference(String sellerPartyReference) {
        this.sellerPartyReference = sellerPartyReference;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentCurrency() {
        return paymentCurrency;
    }

    public void setPaymentCurrency(String paymentCurrency) {
        this.paymentCurrency = paymentCurrency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Trade trade = (Trade) o;

        if (buyerPartyReference != null ? !buyerPartyReference.equals(trade.buyerPartyReference) : trade.buyerPartyReference != null) {
            return false;
        }
        if (sellerPartyReference != null ? !sellerPartyReference.equals(trade.sellerPartyReference) : trade.sellerPartyReference != null) {
            return false;
        }
        if (paymentAmount != null ? !paymentAmount.equals(trade.paymentAmount) : trade.paymentAmount != null) {
            return false;
        }
        return paymentCurrency != null ? paymentCurrency.equals(trade.paymentCurrency) : trade.paymentCurrency == null;
    }

    @Override
    public int hashCode() {
        int result = buyerPartyReference != null ? buyerPartyReference.hashCode() : 0;
        result = 31 * result + (sellerPartyReference != null ? sellerPartyReference.hashCode() : 0);
        result = 31 * result + (paymentAmount != null ? paymentAmount.hashCode() : 0);
        result = 31 * result + (paymentCurrency != null ? paymentCurrency.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Trade{"
                + "buyerPartyReference='" + buyerPartyReference + '\''
                + ", sellerPartyReference='" + sellerPartyReference + '\''
                + ", paymentAmount=" + paymentAmount
                + ", paymentCurrency='" + paymentCurrency + '\''
                + '}';
    }
}
